<?php

class Csv_Importer {
  var $table_name; //where to import to
  var $file_name;  //where to import from
  var $use_csv_header; //use first line of file OR generated columns names
  var $field_separate_char; //character to separate fields
  var $field_enclose_char; //character to enclose fields, which contain separator char into content
  var $field_escape_char;  //char to escape special symbols
  var $error; //error message
  var $arr_csv_columns; //array of columns
  var $table_exists; //flag: does table for import exist
  var $encoding; //encoding table, used to parse the incoming file. Added in 1.5 version
  
  function Csv_Importer($file_name="") {
    $this->file_name = $file_name;
    $this->arr_csv_columns = array();
    $this->use_csv_header = true;
    $this->field_separate_char = ",";
    $this->field_enclose_char  = "\"";
    $this->field_escape_char   = "\\";
    $this->table_exists = false;
  }
  
  /**
   * import()
   * 
   * @global type $databases
   */
  function import()   {
   
    if(empty($this->arr_csv_columns)) {
      $this->get_csv_header_fields();
    }
 
    /*
    if("" != $this->encoding && "default" != $this->encoding) {
      $this->set_encoding();
    }
     * 
     */
    
    if($this->table_exists) {
      db_truncate($this->table_name)->execute();
  
      global $databases;
      $host = $databases['default']['default']['host'];
      $user = $databases['default']['default']['username'];
      $password = $databases['default']['default']['password'];
      $db_name = $databases['default']['default']['database'];
      
      //query paramaters
      $num_lines_ignore = variable_get('csv_importer_ignore_lines');
      $csv_header = ($this->use_csv_header === 'true' ? " IGNORE $num_lines_ignore LINES " :'');
      $escaped_by = " ESCAPED BY '$this->field_escape_char' ";
      
      
      $db = mysqli_connect($host, $user, $password, $db_name);

      $sql = "LOAD DATA INFILE '$this->file_name' INTO TABLE $this->table_name FIELDS TERMINATED BY '$this->field_separate_char' OPTIONALLY ENCLOSED BY '$this->field_enclose_char' $escaped_by $csv_header ";
     
        //   ."(`".implode("`,`", $this->arr_csv_columns)."`)"; //@todo - add csv_columns

      if(!$result = $db->query($sql)){
    die('There was an error running the query [' . $db->error . ']');
} 
    }
  }
  
  /**
   * get_csv_headers_fields()
   * 
   * Returns array of CSV file columns
   *  
   * @return type
   */
  function get_csv_header_fields() {
    $this->arr_csv_columns = array();
    $fpointer = fopen($this->file_name, "r");
    if ($fpointer) {
      $arr = fgetcsv($fpointer, 10*1024, $this->field_separate_char);
      if(is_array($arr) && !empty($arr))  {
        if($this->use_csv_header) {
          foreach($arr as $val)
            if(trim($val)!="")
              $this->arr_csv_columns[] = $val;
        }
        else {
          $i = 1;
          foreach($arr as $val)
            if(trim($val)!="")
              $this->arr_csv_columns[] = "column".$i++;
        }
      }
      unset($arr);
      fclose($fpointer);
    }
    else{
      //$this->error = "file cannot be opened: ".(""==$this->file_name ? "[empty]" : @mysql_escape_string($this->file_name));
      watchdog('csv_upload','File canot be opened: %file_name', array('%file_name' => $this->file_name, ),WATCHDOG_NOTICE);
    }
    return $this->arr_csv_columns;
  }
  
  
  /*
  //returns recordset with all encoding tables names, supported by your database
  function get_encodings()   {
    $rez = array();
    $sql = "SHOW CHARACTER SET";
    $res = @mysql_query($sql);
    if(mysql_num_rows($res) > 0) {
      while ($row = mysql_fetch_assoc ($res)) {
        $rez[$row["Charset"]] = ("" != $row["Description"] ? $row["Description"] : $row["Charset"]); //some MySQL databases return empty Description field
      }
    }
    return $rez;
  }
  
  //defines the encoding of the server to parse to file
  function set_encoding($encoding="") {
    if("" == $encoding)
      $encoding = $this->encoding;
    $sql = "SET SESSION character_set_database = " . $encoding; //'character_set_database' MySQL server variable is [also] to parse file with rigth encoding
    $res = @mysql_query($sql);
    return mysql_error();
  }
  
*/
}
