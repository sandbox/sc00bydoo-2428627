Module Developed by themic8

**About**
Currently the PDO layer for mysql does not have the drivers for running a query with 'LOAD DATA INFILE.' 
This module takes advantage of mysqli_connect to perform this operation


**Installation**
Install like a normal module
You will need to update the Drupal database user's permissions to have FILE.
E.g. GRANT FILE ON * . * TO  'username'@'localhost' IDENTIFIED BY  'password' ;

**Configuration Page**
admin/config/csv-importer

**Notes**
Currently this has been tested to upload a csv directly to a database table without csv headers
When inserting into the database the mysqli_connect use the default database

**Credits**
This is a port over from a class on phpclasses.org
Source: http://www.phpclasses.org/package/2917-PHP-Import-CSV-data-into-a-MySQL-database-table.html