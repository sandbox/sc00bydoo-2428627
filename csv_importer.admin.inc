<?php
/**
 * @file
 * CSV Importer Admin Configuration
 */

/**
 *  Generates form for admin settings
 * 
 * @param type $form
 * @param type $form_state
 * @return string
 */
function csv_importer_admin_settings_form($form, &$form_state) {
  $form['csv_importer_fields']['dbtable'] = array(
    '#type' => 'textfield',
    '#title' => t("Table name"),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => variable_get('csv_importer_table'),
  );
  
  $form['csv_importer_fields']['encoding'] = array(
    '#type' => 'textfield',
    '#title' => t("Encoding"),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => variable_get('csv_importer_encoding', 'UTF-8'),
  );
  
  $form['csv_importer_fields']['separate_char'] = array(
    '#type' => 'textfield',
    '#title' => t("Separate Character"),
    '#size' => 2,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => variable_get('csv_importer_separate_char', ','),
  );
  
  $form['csv_importer_fields']['enclose_char'] = array(
    '#type' => 'textfield',
    '#title' => t("Enclosed Character"),
    '#size' => 2,
    '#maxlength' => 128, 
    '#required' => TRUE,
    '#default_value' => variable_get('csv_importer_enclose_char', '"'),
  );
  
  $form['csv_importer_fields']['escape_char'] = array(
    '#type' => 'textfield',
    '#title' => t("Escape Character"),
    '#size' => 2,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => variable_get('csv_importer_escape_char', "\\"),
  );
  
  $form['csv_importer_fields']['csv_header'] = array(
    '#type' => 'radios',
    '#title' => t("CSV has header"),
    '#options' => array('true' => 'Yes', 'false' => 'No'),
    '#required' => TRUE,
    '#default_value' => variable_get('csv_importer_header', 'false'),
  ); 
  
  $form['csv_importer_fields']['ignore_lines'] = array(
    '#type' => 'textfield',
    '#title' => t("Number of lines to ignore if header is included"),
    '#size' => 2,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => variable_get('csv_importer_ignore_lines', "3"),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Configuration',
  );

  return $form;
  
}

/**
 * Submit handler for form
 * 
 * @param type $form
 * @param type $form_state
 */
function csv_importer_admin_settings_form_submit($form, &$form_state) {

  form_state_values_clean($form_state);
  
  variable_set('csv_importer_table', $form_state['values']['dbtable']);
  variable_set('csv_importer_encoding', $form_state['values']['encoding']);
  variable_set('csv_importer_separate_char', $form_state['values']['separate_char']);
  variable_set('csv_importer_enclose_char', $form_state['values']['enclose_char']);
  variable_set('csv_importer_escape_char', $form_state['values']['escape_char']);
  variable_set('csv_importer_header', $form_state['values']['csv_header']);
  variable_set('csv_importer_ignore_lines', $form_state['values']['ignore_lines']);
  
  drupal_set_message(t('Settings have been saved for the CSV Importer.')); //let user know the settings have been saved
  watchdog('csv_importer_admin', 'The CSV Importer settings have been updated.', array(), WATCHDOG_NOTICE);  //log in watchdog to keep a log of events

}