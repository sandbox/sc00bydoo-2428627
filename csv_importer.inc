<?php
/**
 * @file
 * CSV Importer inc file,
 * This is the page that does the actual importing
 */

include "csv_importer.class.php";

/** 
 * Generates form to import CSV
 * 
 * @param type $form
 * @param type $form_state
 * @return type
 */
function csv_importer_form($form, &$form_state) {

  // Use the #managed_file FAPI element to upload a file.
  $form['csv_importer_fild'] = array(
    '#title' => t('CSV File Upload'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded CSV file will be processed.'),
    '#default_value' => variable_get('csv_importer_fild', ''),
    '#upload_validators' => array(
      'file_validate_extensions' => array('csv'),
    ),
    '#upload_location' => 'public://csv_importer/',
    '#process' => array('cvs_import_disable_upload_button_process'),
);
 
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import New CSV'),
  );
 
  return $form;
}

/** 
 * Handles CSV Submission
 * 
 * @param type $form
 * @param type $form_state
 */
function csv_importer_form_submit($form, &$form_state) {
  // Load the file.
  $file = file_load($form_state['values']['csv_importer_fild']);
  // Change status to permanent.
  $file->status = FILE_STATUS_PERMANENT;
  // Save.
  $uploaded = file_save($file); //e will save the file to have a copy on had on the server.
  if ($uploaded == TRUE) {
    drupal_set_message(t('The file has been successfully uploaded.'));
    //put csv import function here
    $file_path = drupal_realpath($uploaded->uri);  //uploaded is the same as the loaded $file above

    $csv = new csv_importer();

    $csv->file_name = $file_path;
  
    //optional parameters
    $csv->table_name = variable_get('csv_importer_table');
    $csv->use_csv_header = variable_get('csv_importer_header'); // false; //make true?
    $csv->field_separate_char = variable_get('csv_importer_separate_char'); // ',';
    $csv->field_enclose_char =   variable_get('csv_importer_enclose_char'); //'\"';
    $csv->field_escape_char =  variable_get('csv_importer_escape_char'); // '\\';
    $csv->encoding = variable_get('csv_importer_encoding'); //'UTF-8';
    //  $csv->arr_csv_columns = $csv_columns;
    $csv->table_exists = TRUE; 
 
    //start import now
    $csv->import();
 
  }
  else {
    drupal_set_message(t('The file could not be uploaded. Please contact the site administrator.'), 'error');
    watchdog('csv_importer_submit', 'The file could not be uploaded..', array(), WATCHDOG_ERROR);  //log in watchdog to keep a log of events
  }
  
}

/** 
 * Disables the default upload button that comes with this #managed_file form 
 * 
 * @param type $element
 * @param type $form_state
 * @param type $form
 * @return boolean
 */
function cvs_import_disable_upload_button_process($element, &$form_state, $form) { //@todo - update
  $element = file_managed_file_process($element, $form_state, $form); //@todo - update
  $element['upload_button']['#access'] = FALSE;
 
return $element;
}